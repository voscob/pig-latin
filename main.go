package main

import (
	"bufio"
	"fmt"
	"os"

	piglatin "github.com/pig-latin/services"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter text: ")
	text, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Ooops. Something went wrong :(")
	}

	fmt.Println("Pig Latin:", piglatin.Encode(text))
}
