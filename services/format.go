package piglatin

import (
	"strings"
	"unicode"
)

var vowelsEncoded = map[rune]bool{
	'a': true,
	'e': true,
	'i': true,
	'o': true,
	'u': true,
}

// Encode encodes text to Pig Latin.
func Encode(text string) string {
	text = strings.ToLower(text)
	objSl := strings.Split(text, " ")

	resSl := make([]string, len(objSl))
	for k, v := range objSl {
		word, chars := splitLastChars(v)

		if len(word) > 0 {
			if _, ok := vowelsEncoded[rune(word[0])]; ok {
				word = encodeVowelType(word)
			} else if len(word) > 1 {
				if _, ok := vowelsEncoded[rune(word[1])]; !ok {
					word = encodeSingleConsonantType(word)
				} else {
					word = encodeMultipleConsonantsType(word)
				}
			}
		}

		if len(chars) > 0 {
			word = strings.Join([]string{word, chars}, "")
		}
		resSl[k] = word
	}

	return strings.Join(resSl, " ")
}

// splitLastChars slices obj and returns
// a string of letters and the last characters.
func splitLastChars(obj string) (string, string) {
	n := len(obj)
	for i := len(obj) - 1; i >= 0; i-- {
		if !unicode.IsLetter(rune(obj[i])) {
			n = i
		} else {
			break
		}
	}

	if n == 0 {
		return "", obj
	} else if n == len(obj) {
		return obj, ""
	} else {
		return obj[:n], obj[n:]
	}
}

// encodeVowelType encodes word to Pig Latin
// witch begins with a vowel.
func encodeVowelType(word string) string {
	resSl := make([]uint8, len(word)+2)

	j := 0
	for i := 0; i < len(word); i++ {
		resSl[j] = word[i]
		j++
	}

	resSl[len(resSl)-2] = uint8('a')
	resSl[len(resSl)-1] = uint8('y')

	return string(resSl)
}

// encodeSingleConsonantType encodes word to Pig Latin
// witch begins with a single consonant.
func encodeSingleConsonantType(word string) string {
	resSl := make([]uint8, len(word)+2)

	j := 0
	for i := 2; i < len(word); i++ {
		resSl[j] = word[i]
		j++
	}

	resSl[j] = word[0]
	resSl[j+1] = word[1]

	resSl[len(resSl)-2] = uint8('a')
	resSl[len(resSl)-1] = uint8('y')

	return string(resSl)
}

// encodeMultipleConsonantsType encodes word to Pig Latin
// witch begins with multi consonants.
func encodeMultipleConsonantsType(word string) string {
	resSl := make([]uint8, len(word)+2)

	j := 0
	for i := 1; i < len(word); i++ {
		resSl[j] = word[i]
		j++
	}

	resSl[j] = word[0]

	resSl[len(resSl)-2] = uint8('a')
	resSl[len(resSl)-1] = uint8('y')

	return string(resSl)
}
