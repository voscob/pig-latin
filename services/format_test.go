package piglatin

import (
	"fmt"
	"testing"
)

func TestEncode(t *testing.T) {
	exp := "igpay $$$ ananabay, ilesmay: oveglay^ eatay & al'1waysay..."
	pl := Encode("pig $$$ banana, smile: glove^ eat & al'1ways...")
	if pl != exp {
		t.Error("Got:", pl, "expected:", exp)
	}
}

func ExampleEncode() {
	fmt.Println(Encode("pig $$$ banana, smile: glove^ eat & al'1ways..."))
	// Output:
	// igpay $$$ ananabay, ilesmay: oveglay^ eatay & al'1waysay...
}

func BenchmarkEncode(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Encode("pig $$$ banana, smile: glove^ eat & al'1ways...")
	}
}
